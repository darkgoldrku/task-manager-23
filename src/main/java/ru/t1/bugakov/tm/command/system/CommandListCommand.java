package ru.t1.bugakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.service.ICommandService;
import ru.t1.bugakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final ICommandService commandService = getCommandService();
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@Nullable AbstractCommand command : commands) {
            if (command == null) continue;
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }

}
