package ru.t1.bugakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.ICommandService;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
