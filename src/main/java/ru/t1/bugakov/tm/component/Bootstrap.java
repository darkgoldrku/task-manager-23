package ru.t1.bugakov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.ICommandRepository;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.command.project.*;
import ru.t1.bugakov.tm.command.system.*;
import ru.t1.bugakov.tm.command.task.*;
import ru.t1.bugakov.tm.command.user.*;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.bugakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.repository.CommandRepository;
import ru.t1.bugakov.tm.repository.ProjectRepository;
import ru.t1.bugakov.tm.repository.TaskRepository;
import ru.t1.bugakov.tm.repository.UserRepository;
import ru.t1.bugakov.tm.service.*;
import ru.t1.bugakov.tm.util.TerminalUtil;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindToProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserRemoveCommand());
        registry(new UserUnlockCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(admin.getId(), "p111", "222");
        taskService.create(admin.getId(), "t001", "222");
        taskService.create(admin.getId(), "t002", "444");

        @NotNull User test = userService.create("test", "test", "test@tast.ru");
        projectService.create(test.getId(), "p333", "444");
        taskService.create(test.getId(), "t003", "222");
        taskService.create(test.getId(), "t004", "444");

        userService.create("user", "user", "user@user.ru");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("TASK MANAGER IS SHUTTING DOWN")));
    }

    public void start(@NotNull final String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

}