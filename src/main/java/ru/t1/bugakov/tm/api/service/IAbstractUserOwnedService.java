package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

public interface IAbstractUserOwnedService<M extends AbstractUserOwnedModel> extends IAbstractUserOwnedRepository<M> {


}
