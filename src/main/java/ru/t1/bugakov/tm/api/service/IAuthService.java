package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email);

    void login(@Nullable final String login, @Nullable final String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @Nullable
    User getUser();

    void checkRoles(@Nullable final Role[] roles);

}
