package ru.t1.bugakov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! This argument is not supported.");
    }

    public ArgumentNotSupportedException(final String arg) {
        super("Error! This argument - \"" + arg + "\" is not supported.");
    }

}
