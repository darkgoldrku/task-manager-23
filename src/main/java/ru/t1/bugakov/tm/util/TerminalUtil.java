package ru.t1.bugakov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
